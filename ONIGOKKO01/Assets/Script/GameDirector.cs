﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }


   private void OnCollisionEnter(Collision collision)
    {
        //playerが衝突したオブジェクトが階段だった場合
        if (collision.gameObject.name == "Step")
        {
            SceneManager.LoadScene("MazeScene");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

 
}
